#ifndef ASSIGNMENT3_PPMOUTPUT_H
#define ASSIGNMENT3_PPMOUTPUT_H

#include <iostream>
#include <fstream>
#include <vector>
#include <bits/stdc++.h>
#include <string>

using namespace std;

class PPMOutput {
public:
    PPMOutput();

    void CreateFile(string file_name, vector<vector<tuple<int, int, int>>> RGBvals);
};


#endif
