a.out: main.o PixelWidget.o PPMOutput.o
	g++ -std=c++11 main.o PixelWidget.o PPMOutput.o

main.o: main.cpp
	g++ -std=c++11 -c main.cpp

PixelWidget.o: PixelWidget.cpp PixelWidget.h
	g++ -std=c++11 -c PixelWidget.cpp

PPMOutput.o: PPMOutput.cpp PPMOutput.h
	g++ -std=c++11 -c PPMOutput.cpp

