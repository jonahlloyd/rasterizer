#include "PixelWidget.h"

PPMOutput ppm_file;

PixelWidget::PixelWidget(int triangleCoords[3][2], int colours[3][3], int background[3], float UV[3][2]) {

    // Background colour
    backgroundColour[0] = background[0];
    backgroundColour[1] = background[1];
    backgroundColour[2] = background[2];

    // Copy triangle coordinates
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 2; ++j) {
            coords[i][j] = triangleCoords[i][j];
        }
    }
    // Copy triangle coord colours
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            triangleColour[i][j] = colours[i][j];
        }
    }
    // Copy triangle UV coords
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 2; ++j) {
            triangleUV[i][j] = UV[i][j];
        }
    }
    SetBackground();
    // Area of whole triangle
    denominator = (((float) coords[1][1] - (float) coords[2][1]) * ((float) coords[0][0] - (float) coords[2][0]) +
                   ((float) coords[2][0] - (float) coords[1][0]) * ((float) coords[0][1] - (float) coords[2][1]));
}

void PixelWidget::DrawImage() {
    SetBackground();
    int i = 0;
    for (int y = 127; y >= 0; --y) { // The image is draw from the top left
        for (int x = 0; x < 128; ++x) {

            // alpha
            alpha = ((((float) coords[1][1] - (float) coords[2][1]) * ((float) x - (float) coords[2][0])) +
                     (((float) coords[2][0] - (float) coords[1][0]) * ((float) y - (float) coords[2][1]))) /
                    denominator;
            // beta
            beta = (((float) coords[2][1] - (float) coords[0][1]) * ((float) x - (float) coords[2][0]) +
                    ((float) coords[0][0] - (float) coords[2][0]) * ((float) y - (float) coords[2][1])) / denominator;
            // gamma
            gamma = 1.0f - alpha - beta;

            if (HalfPlaneTest()) {
                get<0>(RGBvals[i][x]) = RGB[0];
                get<1>(RGBvals[i][x]) = RGB[1];
                get<2>(RGBvals[i][x]) = RGB[2];
            }
        }
        ++i; // Use i to count so we increment in the vector
    }

    ppm_file.CreateFile("output/texture", RGBvals);
}

bool PixelWidget::HalfPlaneTest() {
    // Check pixel is inside triangle
    if (alpha >= 0 && beta >= 0 && gamma >= 0) {
        TextInterpolation();
        return true;
    } else {
        return false;
    }
}

void PixelWidget::ReadPPM() {
    ifstream input("input/earth.ppm");

    // Skip first 2 lines
    getline(input, line);
    getline(input, line);
    // Get image resolution
    input >> imageSize[0] >> imageSize[1];
    // Skip max rgb value line
    getline(input, line);
    getline(input, line);

    for (int y = 0; y < imageSize[1]; ++y) {
        inputRGBvals.push_back(vector<tuple<int, int, int>>());
        for (int x = 0; x < imageSize[0]; ++x) {
            // Store next 3 values as tuple in 2d vector
            input >> inputRGB[0] >> inputRGB[1] >> inputRGB[2];
            inputRGBvals[y].push_back(tuple<int, int, int>(inputRGB[0], inputRGB[1], inputRGB[2]));
        }
    }
}

void PixelWidget::UVTextCoords() {
    for (int i = 0; i < 3; ++i) {
        uvCoords[i][0] = triangleUV[i][0] * (float) inputRGBvals[0].capacity(); // Find x in texture in relation to UV
        uvCoords[i][1] = triangleUV[i][1] * (float) inputRGBvals.capacity(); // Find x in texture in relation to UV
    }
}

void PixelWidget::TextInterpolation() {
    textureX = (float) uvCoords[0][0] * alpha + (float) uvCoords[1][0] * beta + (float) uvCoords[2][0] * gamma;
    textureY = (float) uvCoords[0][1] * alpha + (float) uvCoords[1][1] * beta + (float) uvCoords[2][1] * gamma;
    // Find RGB values for the location on the texture
    RGB[0] = get<0>(inputRGBvals[textureY][textureX]);
    RGB[1] = get<1>(inputRGBvals[textureY][textureX]);
    RGB[2] = get<2>(inputRGBvals[textureY][textureX]);
}

void PixelWidget::SetBackground() {
    // Set image to background colour
    for (int i = 0; i < 128; ++i) { //Image drawn from top left
        RGBvals.push_back(vector<tuple<int, int, int>>());
        for (int j = 0; j < 128; ++j) {
            RGBvals[i].push_back(tuple<int, int, int>(backgroundColour[0], backgroundColour[1], backgroundColour[2]));
        }
    }
}
