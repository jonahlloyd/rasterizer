#include "PPMOutput.h"

PPMOutput::PPMOutput() {}

void PPMOutput::CreateFile(string file_name, vector<vector<tuple<int, int, int>>> RGBvals) {

    ofstream output(file_name + ".ppm");

    // Creating header
    output << "P3" << endl;
    output << "#." << endl;
    output << 128 << " " << 128 << endl;
    output << "255" << endl;
    // RGB Data
    for (int i = 0; i < 128; ++i) {
        for (int j = 0; j < 128; ++j) {
            output << get<0>(RGBvals[i][j]) << " " << get<1>(RGBvals[i][j]) << " " << get<2>(RGBvals[i][j]);
            output << "  ";
        }
        output << endl;
    }
}
